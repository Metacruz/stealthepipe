/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stealthepile;

import ComplementariesClasses.Card;
import ComplementariesClasses.InitGame;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author RonaldVelezB, GabrielaCuadrosC, MelanieCruzR
 */
public class StealThePile extends Application {

    public static TreeMap resultAllGames = new TreeMap();
    public static int totalTimeSeconds = 0;
    public static int victories = 0;
    public static int defeats = 0;
    //info de la ultima partida;
    public static int playerPoints = 0;
    public static int opponentPoints = 0;
    public static int pointsDif = 0;
    public static String timeLastGame = "";
    public static int pilesStealed = 0;

    //info para estadisticas generales
    public static int playerTotalPoints = 0;
    public static int opponentTotalPoints = 0;
    public static int totalPointsDif = 0;
    public static int totalGames = 0;
    public static int ties = 0;
    public static double victoriesPerc;

    public static String averageTime = "00:00:00";
    public static int totalPilesStealed = 0;

    public static int cardsS = 0;
    public static final int X = 900;
    public static final int Y = 700;
    public static final int x = 100;
    public static final int y = 165;
    public static int time = 0;
    public static Stage stg;
    public static String noFlippedCard = "/CardsModels/Poker/Volteada/back1.png";
    public static ArrayList<Card> cards = new ArrayList<>();
    public static String[] poker = {"Treboles", "Espadas", "Corazones", "Brillos"};
    public static int sizePoker = 13;
    public static String pokerU = "Poker";
    public static String[] espaniolas = {"bastos", "copas", "espadas", "oros"};
    public static int sizeEspaniolas = 13;
    public static String espaniolasU = "Espaniolas";
    public static ArrayList<Card> cardsSelected = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        deserializeData();
        if (totalGames == 0) {
            victoriesPerc = 0;
        } else {
            victoriesPerc = (victories / totalGames) * 100;
        }
        createCards(poker, sizePoker, pokerU);
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stg = primaryStage;
        InitGame ig = new InitGame();
        Scene sc = new Scene(ig.getRoot(), X, Y);
        sc.getStylesheets().add("/Styles/GlobalStyles.css");
        stg.setScene(sc);
        stg.setTitle("Steal The Pile Game");
        stg.show();
    }

    public static void createCards(String[] kind, int size, String type) {
        if (cards.size() > 0) {
            cards.clear();
        }
        for (String s : kind) {
            for (int i = 0; i < size; i++) {
                Card c = new Card(i + 1, s, type);
                cards.add(c);
            }

        }
    }

    public static void serializeData() {

        FileOutputStream fo = null;
        try {
            fo = new FileOutputStream("src/Data/Data.ser");
            ObjectOutputStream os = new ObjectOutputStream(fo);
            os.writeObject(resultAllGames);
            os.close();
            fo.close();
        } catch (IOException ex) {
        }
    }

    public static void deserializeData() {
        try {
            FileInputStream fi=new FileInputStream("src/Data/Data.ser");
            ObjectInputStream oi=new ObjectInputStream(fi);
            resultAllGames=(TreeMap)oi.readObject();
            oi.close();
            fi.close();
        } catch (FileNotFoundException ex) {
            File f =new File("/Data/Data.ser");
            try {
                f.createNewFile();
            } catch (IOException ex1) {
            }
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }
    }
}
