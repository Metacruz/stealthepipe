/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComplementariesClasses;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import stealthepile.StealThePile;

/**
 *
 * @author ronal
 */
public class Settings implements CommonMethods{
    
    private VBox root;
    private HBox kind;
    private VBox cardPoker;
    private Label tagPoker;
    private VBox cardEsp;
    private Label tagEsp;
    private Button back;
    
    
    public Settings(){
    initParameters();
    arrangeParameters();
    clicks();
    }

    @Override
    public void arrangeParameters() {
        kind.getChildren().addAll(cardPoker,cardEsp);
        ImageView p=new ImageView("/CardsModels/Poker/Espadas/1.png");
        p.setFitHeight(StealThePile.y);
        p.setFitWidth(StealThePile.x);
        cardPoker.getChildren().addAll(p,tagPoker);
        ImageView e=new ImageView("/CardsModels/Espaniolas/oros/1.png");
        e.setFitHeight(StealThePile.y);
        e.setFitWidth(StealThePile.x);
        cardEsp.getChildren().addAll(e,tagEsp);
        root.getChildren().addAll(kind,back);
    }

    @Override
    public void initParameters() {
        root=new VBox();
        back=new Button("Back");
        tagPoker=new Label("Poker");
        tagEsp=new Label("Espaniolas");
        cardEsp=new VBox();
        cardPoker=new VBox();
        kind=new HBox();
    }
        public void clicks(){
        back.setOnAction(r->{
            InitGame ig = new InitGame();
            Scene sc = new Scene(ig.getRoot(),StealThePile.X,StealThePile.Y);
            StealThePile.stg.setScene(sc);
        });
    
        cardEsp.setOnMouseClicked(p->{
            StealThePile.noFlippedCard="/CardsModels/Espaniolas/Volteada/Girada.png";
            StealThePile.cards.clear();
            StealThePile.createCards(StealThePile.espaniolas, StealThePile.sizeEspaniolas,StealThePile.espaniolasU);
        });
        
        cardPoker.setOnMouseClicked(p->{
            StealThePile.noFlippedCard="/CardsModels/Poker/Volteada/back1.png";
            StealThePile.cards.clear();
            StealThePile.createCards(StealThePile.poker, StealThePile.sizePoker,StealThePile.pokerU);
        });
    }

    public VBox getRoot() {
        return root;
    }

    public void setRoot(VBox root) {
        this.root = root;
    }

    public Button getBack() {
        return back;
    }

    public void setBack(Button back) {
        this.back = back;
    }
        
        
    
}
