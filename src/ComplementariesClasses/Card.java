/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComplementariesClasses;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import stealthepile.StealThePile;

/**
 *
 * @author RonaldVelezB, GabrielaCuadrosC, MelanieCruzR
 */
public class Card {

    private VBox root = new VBox();
    private boolean active, selected;
    private Image back, front;
    private int number;
    private ImageView image = new ImageView();

    public Card(int value, String folder, String kind) {
        this.active = false;
        this.back = new Image(StealThePile.noFlippedCard);
        this.front = new Image("/CardsModels/" + kind + "/" + folder + "/" + value + ".png");
        this.number = value;
        image.setImage(back);
        root.getChildren().add(image);
        selected = false;

        image.setFitHeight(StealThePile.y);
        image.setFitWidth(StealThePile.x);

        image.setOnMouseClicked(p -> {
             if (StealThePile.cardsSelected.size() == 1 & Game.gameArea.getChildren().size() == 0) {
                Game.gameArea.add(Game.cardsPlayer.getChildren().get(0), 0, 0);
                Game.turnEnded = true;
                StealThePile.cardsSelected.clear();
            }
            if (StealThePile.cardsSelected.size() == 0) {
                StealThePile.cardsSelected.add(this);
                Game.gameArea.setDisable(false);
                Game.cardsPlayer.setDisable(true);
                Game.cardsComputer.setDisable(false);
            }  else if (StealThePile.cardsSelected.size() == 1) {
                StealThePile.cardsSelected.add(this);
                stealCard();
            }
        });

    }

    public void stealCard() {
        if (StealThePile.cardsSelected.size() == 2) {
            boolean cont1=true;
            if(Game.pileComputer.getChildren().size()>0){
                
                Card c= (Card)Game.pileComputer.getChildren().get(Game.pileComputer.getChildren().size()-1).getUserData();
            if(c.getNumber()==StealThePile.cardsSelected.get(0).getNumber()){
                
                StealThePile.pilesStealed+=1;
                Game.pileComputer.getChildren().add(
                StealThePile.cards.get(StealThePile.cards.indexOf(StealThePile.cardsSelected.get(0))).getRoot()
                );
                Game.pilePlayer.getChildren().addAll(
                Game.pileComputer.getChildren()
                );
                
                
                cont1=false;
                
            }
            }
            
            boolean cont2=true;
            if(cont1){
            if (StealThePile.cardsSelected.get(0).getNumber() == StealThePile.cardsSelected.get(1).getNumber()) {
                Game.pilePlayer.getChildren().addAll(
                        StealThePile.cards.get(StealThePile.cards.indexOf(StealThePile.cardsSelected.get(1))).getRoot(),
                        StealThePile.cards.get(StealThePile.cards.indexOf(StealThePile.cardsSelected.get(0))).getRoot()
                );
                cont2=false;

            }}
            
            if(cont2) {
                for (int i = 0; i <= 1; i++) {
                    for (int j = 0; j <= 6; j++) {
                        try {
                            if (getNodeFromGridPane(Game.gameArea, j, i) == null) {
                                Game.gameArea.add(StealThePile.cardsSelected.get(0).getRoot(), j, i);
                                break;
                            }
                        } catch (NullPointerException | IllegalArgumentException e) {

                        }
                    }
                }


            }

            StealThePile.cardsSelected.clear();
            Game.turnEnded = true;
            Game.gameArea.setDisable(true);
            Game.cardsPlayer.setDisable(false);
        }
    }

    public static Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    public void turn() {

        image.setImage(front);
        active = true;

    }

    public void cardsShowBack() {
        if (active == true) {
            image.setImage(back);
            active = false;
        }
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Image getBack() {
        return back;
    }

    public void setBack(Image back) {
        this.back = back;
    }

    public Image getFront() {
        return front;
    }

    public void setFront(Image front) {
        this.front = front;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public VBox getRoot() {
        return root;
    }

    public void setRoot(VBox root) {
        this.root = root;
    }

}
