/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComplementariesClasses;

import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.util.Collections;
import javafx.scene.layout.StackPane;
import stealthepile.StealThePile;
import static ComplementariesClasses.Card.getNodeFromGridPane;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;

/**
 *
 * @author ronal
 */
public class Game implements CommonMethods, Runnable {

    public static boolean turnEnded = false;
    final int reference = StealThePile.cards.size();
    private VBox root, crdC, centerL, centerR;
    private Button end;
    public static GridPane gameArea;
    public static HBox top, bottom, cardsPlayer, cardsComputer, center;
    public static StackPane cards, pilePlayer, pileComputer;
    public boolean cardPS = false;
    private Label time;
    private boolean ejecute = true;

    public Game() throws InterruptedException {
        initParameters();
        arrangeParameters();
        putCards();
        clickEnd();
    }

    public void stopThreads() {
        ejecute = false;
    }

    public void initParameters() {
        root = new VBox();
        end = new Button("End Game");
        gameArea = new GridPane();
        top = new HBox();
        bottom = new HBox();
        cardsPlayer = new HBox();
        cardsComputer = new HBox();
        center = new HBox();

        //stackPanes
        pilePlayer = new StackPane();
        pileComputer = new StackPane();

        cards = new StackPane();
        crdC = new VBox();
        centerL = new VBox();
        centerR = new VBox();
        time = new Label("Tiempo Transcurrido: ");
        gameArea.setPrefSize(560, 400);
        pilePlayer.setPrefSize(StealThePile.x, StealThePile.y);
        pileComputer.setPrefSize(StealThePile.x, StealThePile.y);
        cardsPlayer.setPrefSize(3.5 * StealThePile.x, StealThePile.y);
        cardsComputer.setPrefSize(3.5 * StealThePile.x, StealThePile.y);
        cardsPlayer.setPadding(new Insets(15));
        cardsComputer.setPadding(new Insets(15));
        cardsComputer.setDisable(true);//se desabilita la seleccion de las cartas de la computadora
        cards.setDisable(true);//se deshabilita la seleccion de las cartas del mazo
        gameArea.setDisable(true);

    }

    @Override
    public void arrangeParameters() {
        crdC.getChildren().addAll(cards, time, end);
        top.getChildren().addAll(cardsComputer, pileComputer);
        centerR.getChildren().addAll(crdC);
        centerL.getChildren().addAll(gameArea);
        center.getChildren().addAll(centerL, centerR);
        bottom.getChildren().addAll(pilePlayer, cardsPlayer);
        root.getChildren().addAll(top, center, bottom);
    }

    public void handlerTurns() {
        if (true == turnEnded) {
            cardsPlayer.setDisable(true);
            Card mano = null;
            Card mesa = null;
            boolean cont = true;
            boolean cont1 = true;
            boolean cut = false;
            for (Iterator<Node> it = cardsComputer.getChildren().iterator(); it.hasNext();) {
                Node d = it.next();
                if (Game.pilePlayer.getChildren().size() > 0) {

                    Card c = (Card) Game.pilePlayer.getChildren().get(Game.pilePlayer.getChildren().size() - 1).getUserData();
                    Card n = (Card) d.getUserData();
                    if (c.getNumber() == n.getNumber()) {
                        Platform.runLater(() -> {
                            Game.pileComputer.getChildren().add(
                                    StealThePile.cards.get(StealThePile.cards.indexOf(n)).getRoot()
                            );
                            Game.pileComputer.getChildren().addAll(
                                    Game.pilePlayer.getChildren()
                            );
                        });

                        cont1 = false;
                        cont = false;
                        cut = true;
                    }
                }
                if (cont1) {
                    for (Node n : gameArea.getChildren()) {
                        mano = (Card) d.getUserData();
                        mesa = (Card) n.getUserData();
                        if (mesa.getNumber() == mano.getNumber()) {
                            mano.turn();
                            mesa.turn();
                            Platform.runLater(() -> {
                                pileComputer.getChildren().addAll(n, d);
                                cardsComputer.getChildren().remove(d);
                                gameArea.getChildren().remove(n);
                            });
                            cut = true;
                            cont = false;
                            break;
                        }
                    }
                    if (cut) {
                        break;
                    }
                }
            }

            if (cont) {
                for (int i = 0; i <= 1; i++) {
                    cut = false;
                    for (int j = 0; j <= 6; j++) {
                        try {
                            final int a = i;
                            final int b = j;
                            if (getNodeFromGridPane(Game.gameArea, j, i) == null) {
                                Card c = (Card) cardsComputer.getChildren().get(0).getUserData();
                                c.turn();
                                Platform.runLater(() -> {
                                    gameArea.add(cardsComputer.getChildren().get(0), b, a);

                                });
                                cut = true;
                                break;
                            }
                        } catch (NullPointerException | IllegalArgumentException e) {

                        }
                    }
                    if (cut) {
                        break;
                    }
                }
            }

            turnEnded = false;
            cardsPlayer.setDisable(false);

        }
    }

    @Override
    public void run() {
        while (ejecute) {
            try {
                dealCards();
                timer();
            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Este metodo termina el juego cuando ya no hay mas movimientos por hacer
     */
    public void endGameAutomatically() {

        if (cardsPlayer.getChildren().isEmpty() & cardsComputer.getChildren().isEmpty() & cards.getChildren().isEmpty()) {
            String result = "";
            StealThePile.playerPoints += pilePlayer.getChildren().size();
            StealThePile.opponentPoints += pileComputer.getChildren().size();
            StealThePile.pointsDif = StealThePile.opponentPoints - StealThePile.playerPoints;
            if (StealThePile.pointsDif < 0) { //si la diferencia sale negativa se la convierte a un numero positivo
                StealThePile.pointsDif = StealThePile.pointsDif * (-1);
            }

            if (StealThePile.playerPoints > StealThePile.opponentPoints) {
                result = "victory";
            } else if (StealThePile.playerPoints < StealThePile.opponentPoints) {
                result = "defeat";
            } else {
                result = "tie";
            }
            StealThePile.resultAllGames.put(result, new GameResume(StealThePile.playerPoints, StealThePile.opponentPoints,
                    StealThePile.time, StealThePile.pilesStealed));
            Platform.runLater(() -> {
                GameEnded i = new GameEnded();
                Scene s = new Scene(i.getRoot(), StealThePile.X, StealThePile.Y);
                StealThePile.stg.setScene(s);

                stopThreads();

            });

            
        }

    }

    public void timer() throws InterruptedException {

        for (int hours = 0; hours < 60; hours++) {
            for (int minutes = 0; minutes < 60; minutes++) {
                for (int seconds = 0; seconds < 60; seconds++) {
                    final int sec = seconds;
                    final int min = minutes;
                    final int hr = hours;

                    StealThePile.totalTimeSeconds += 1;
                    Platform.runLater(() -> {
                        time.setText(String.format("Tiempo transcurrido: %1$02d:%2$02d:%3$02d", hr, min, sec));
                    });
                    StealThePile.time += 1;

                    checkCardsAvaliable();
                    handlerTurns();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {

                    }

                    endGameAutomatically();
                    if (ejecute == false) {
                        break;
                    }
                }
                if (ejecute == false) {
                    break;
                }
            }
            if (ejecute == false) {
                break;
            }
        }

    }

    public void putCards() {
        Collections.shuffle(StealThePile.cards);
        for (Card c : StealThePile.cards) {
            c.getRoot().setUserData(c);
            c.cardsShowBack();
            cards.getChildren().add(c.getRoot());
        }

    }

    public void checkCardsAvaliable() {
        if (cardsPlayer.getChildren().size() == 0 & cardsComputer.getChildren().size() == 0) {
            givingCards();

        }

    }

    public void clickEnd() {
        end.setOnAction(p -> {
            InitGame i = new InitGame();
            Scene s = new Scene(i.getRoot(), StealThePile.X, StealThePile.Y);
            StealThePile.stg.setScene(s);
        });
    }

    public void cardsOnDeck() {
        for (int i = 0; i < 4; i++) {

            final int r = i;
            final int column = i;
            Platform.runLater(() -> {
                Card c = (Card) cards.getChildren().get(r).getUserData();
                c.turn();
                gameArea.add(cards.getChildren().get(r), column, 0);
            });
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {

            }
        }

    }

    public void dealCards() {

        givingCards();
        cardsOnDeck();

    }

    public void givingCards() {
        if (cards.getChildren().size() > 0) {

            for (int i = 0; i < 3; i++) {

                final int r = i;

                Platform.runLater(() -> {
                    try {
                        cardsComputer.getChildren().add(cards.getChildren().get(r));
                    } catch (ArrayIndexOutOfBoundsException e) {

                    }

                });
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {

                }
            }
            for (int i = 0; i < 3; i++) {
                final int r = i;
                if (cards.getChildren().size() == 1) {
                    Platform.runLater(() -> {
                        Card c = (Card) cards.getChildren().get(0).getUserData();
                        c.turn();
                        try {

                            cardsPlayer.getChildren().add(c.getRoot());
                        } catch (ArrayIndexOutOfBoundsException e) {

                        }

                    });
                } else {
                    Platform.runLater(() -> {
                        Card c = (Card) cards.getChildren().get(r).getUserData();
                        c.turn();
                        try {

                            cardsPlayer.getChildren().add(c.getRoot());
                        } catch (ArrayIndexOutOfBoundsException e) {

                        }

                    });
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {

                }

            }

        }
    }

    public VBox getRoot() {
        return root;
    }

}
