/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComplementariesClasses;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import stealthepile.StealThePile;

/**
 *
 * @author ronal
 */
public class Stats implements CommonMethods{
    private Button back;
    private Label totalGames,pointsTG,opponentPointsTG,pointsDiffTG
            ,playedGamesTG,wins,empate,winsPerc,medTime,stealedPilesTG;
    private VBox root;
    
    public Stats(){
        initParameters();
        arrangeParameters();
        clickBack();
    }
    
    public void initParameters(){
    root=new VBox();
    back=new Button("Back");
    
    //
    
    totalGames=new Label("All Games");
    pointsTG=new Label("All your points: "+StealThePile.playerTotalPoints);
    opponentPointsTG=new Label("All Computer's points StealThePile"+StealThePile.opponentTotalPoints);
    pointsDiffTG=new Label("Points difference: "+StealThePile.totalPointsDif);
    playedGamesTG=new Label("Total played games: "+StealThePile.totalGames);
    wins=new Label("Total victories: "+StealThePile.victories);
    empate=new Label("Total ties: "+StealThePile.ties);
    winsPerc=new Label("Victorie's percentaje: "+StealThePile.victoriesPerc);
    medTime=new Label("Medium time per party: "+StealThePile.averageTime);
    stealedPilesTG=new Label("Total piles stealed: "+StealThePile.totalPilesStealed);
    
    
    }
    
    public void arrangeParameters(){
        root.getChildren().addAll(totalGames,pointsTG,opponentPointsTG,pointsDiffTG
            ,playedGamesTG,wins,empate,winsPerc,medTime,stealedPilesTG,
                back);
    }
    
    public void clickBack(){
        back.setOnAction(r->{
            InitGame ig = new InitGame();
            Scene sc = new Scene(ig.getRoot(),StealThePile.X,StealThePile.Y);
            StealThePile.stg.setScene(sc);
        });
    
    }

    public VBox getRoot() {
        return root;
    }
    
    
    
}
