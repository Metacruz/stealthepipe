/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComplementariesClasses;


import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.Glow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import stealthepile.StealThePile;

/**
 *
 * @author ronal
 */
public class InitGame implements CommonMethods{
    public static final double MAX_GLOW_VALUE=0.3;
    public static final double MIN_GLOW_VALUE=0;
    private VBox root;
    private HBox cNewGame, cSettings, cStats, cAbout;
    private Button newGame, settings, stats,exit;
    

    public InitGame() {
        initParameters();
        arrangeParameters();
        buttonsBehavior();
    }

    public void initParameters() {
        root = new VBox();

        cNewGame = new HBox();
        cSettings = new HBox();
        cStats = new HBox();
        cAbout = new HBox();

        newGame = new Button("New Game");
        settings = new Button("Settings");
        stats = new Button("Stats");
        exit=new Button("Exit");
    }

    public void arrangeParameters() {
        cNewGame.getChildren().add(newGame);
        cSettings.getChildren().add(settings);
        cStats.getChildren().add(stats);
        root.getChildren().addAll(cNewGame, cSettings, cStats, cAbout,exit);
    }
    
    public void buttonsBehavior(){
    mouseAboveButtons();
    mouseOutButtons();
    clickButtons();
    }

    public void mouseAboveButtons() {
        newGame.setOnMouseMoved(e -> {
            
            
            newGame.setEffect(new Glow(MAX_GLOW_VALUE));

        });

        settings.setOnMouseMoved(e -> {
            settings.setEffect(new Glow(MAX_GLOW_VALUE));

        });

        stats.setOnMouseMoved(e -> {
            stats.setEffect(new Glow(MAX_GLOW_VALUE));

        });

        

    }

    public void mouseOutButtons() {
        newGame.setOnMouseExited(e -> {
            newGame.setEffect(new Glow(MIN_GLOW_VALUE));
        });

        settings.setOnMouseExited(e -> {
            settings.setEffect(new Glow(MIN_GLOW_VALUE));

        });
        
        stats.setOnMouseExited(e -> {
            stats.setEffect(new Glow(MIN_GLOW_VALUE));

        });
        
        exit.setOnAction(m->{
            System.out.println(StealThePile.resultAllGames.size());
            StealThePile.serializeData();
            Platform.exit();
        });
        
    }
    
    public void clickButtons(){
    stats.setOnAction(r->{
        Stats st = new Stats();
        Scene sc = new Scene(st.getRoot(),StealThePile.X,StealThePile.Y);
        StealThePile.stg.setScene(sc);
    });
    
    newGame.setOnAction(r->{
        Game g;
        try {
            
            g = new Game();
            Scene sc =new Scene(g.getRoot(),StealThePile.X,StealThePile.Y);
            sc.getStylesheets().add("/Styles/GlobalStyles.css");
            Thread threadGame = new Thread(g);
            threadGame.setDaemon(true);
            threadGame.start();
            
        StealThePile.stg.setScene(sc);
        } catch (InterruptedException ex) {
        }
        
    });
    
    settings.setOnAction(r->{
        Settings s;
        s = new Settings();
        Scene sc =new Scene(s.getRoot(),StealThePile.X,StealThePile.Y);
        StealThePile.stg.setScene(sc);
        
    });
    }
    
    

    public VBox getRoot() {
        return root;
    }

}
