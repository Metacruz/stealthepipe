/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComplementariesClasses;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import stealthepile.StealThePile;

/**
 *
 * @author ronal
 */
public class GameEnded {

    private VBox root;

    private Button cont;
    private Label pointsLP, opponentPointsLP, pointsDifferenceLP, timeLP, stealedPilesLP;

    public GameEnded() {
        initParams();
        order();
        clickContinue();
    }

    public void order() {
        root.getChildren().addAll(pointsLP, opponentPointsLP, pointsDifferenceLP, timeLP, stealedPilesLP, cont);
    }

    public void clickContinue() {
        cont.setOnAction(p -> {
            StealThePile.time = 0;
            StealThePile.opponentPoints = 0;
            StealThePile.pointsDif = 0;
            StealThePile.playerPoints = 0;
            StealThePile.pilesStealed = 0;
            InitGame i = new InitGame();
            Scene s = new Scene(i.getRoot(), StealThePile.X, StealThePile.Y);
            StealThePile.stg.setScene(s);

        });
    }

    public void initParams() {
        root = new VBox();
        cont = new Button("Continue");
        pointsLP = new Label("Your points: " + StealThePile.playerPoints);
        opponentPointsLP = new Label("Computer's points: " + StealThePile.opponentPoints);
        pointsDifferenceLP = new Label("Points Difference: " + StealThePile.pointsDif);
        timeLP = new Label("Time: " + StealThePile.time + " s");
        stealedPilesLP = new Label("Piles Stealed: " + StealThePile.pilesStealed);
    }

    public VBox getRoot() {
        return root;
    }

}
