/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComplementariesClasses;

import java.io.Serializable;

/**
 *
 * @author ronal
 */
public class GameResume implements Serializable{
    private int pointsPlayer;
    private int pointsOpponent;
    private int time;
    private int stealedPiles;

    public GameResume(int pointsPlayer, int pointsOpponent, int time, int stealedPiles) {
        this.pointsPlayer = pointsPlayer;
        this.pointsOpponent = pointsOpponent;
        this.time = time;
        this.stealedPiles = stealedPiles;
    }

    public int getPointsPlayer() {
        return pointsPlayer;
    }

    public void setPointsPlayer(int pointsPlayer) {
        this.pointsPlayer = pointsPlayer;
    }

    public int getPointsOpponent() {
        return pointsOpponent;
    }

    public void setPointsOpponent(int pointsOpponent) {
        this.pointsOpponent = pointsOpponent;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getStealedPiles() {
        return stealedPiles;
    }

    public void setStealedPiles(int stealedPiles) {
        this.stealedPiles = stealedPiles;
    }
    
    
}
